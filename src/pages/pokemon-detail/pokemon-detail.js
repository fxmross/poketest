import React, { useEffect, useState } from "react";
import { useParams } from "react-router-dom";
import { useNavigate } from "react-router-dom";
import Axios from "axios";
import Stats from "../../components/stats/stats";
import StatsGraph from "../../components/stats-graph/stats-graph";
import './pokemon-detail.scss'
function PokemonDetail(props) {
  const navigate = useNavigate();

  let { id } = useParams();

  const [Pokemon, setPokemon] = useState({});
  const handleBack = () => {
    navigate("/");
  };
  useEffect(() => {
    (async () => {
      const url = `https://pokeapi.co/api/v2/pokemon/${id}`;
      await Axios.get(`${url}`)
        .then((data) => {
          if (data && data.data) {
            setPokemon(data.data);
          }
        })
        .catch((error) => {
          return null;
        });
    })();
  }, []);

  const renderId = () => {
    let tempId = "" + Pokemon.id;
    for (var i = 3; i >= tempId.length; i--) {
      tempId = "0" + tempId;
    }
    return tempId;
  };
  return (
    <div className="container-fluid pokemon_detail ">
      <div className="row d-flex pt-2 bg-primary">
        <div className="col-3">
          <button className="btn btn-warning" onClick={() => handleBack()}>
            Volver
          </button>
        </div>
        <div className="col-6 text-center ">
          <div>
            {" "}
            <h1>
              #{renderId()} - {Pokemon.name}
            </h1>{" "}
          </div>
        </div>
      </div>
   
        <div className="col-12 col-md-12 d-sm-block d-md-flex py-5">
          <div className="col-lg-6 col-md-6 col-sm-12  bg-white border border-2 rounded-3">
            <img
              alt=""
              width={"100%"}
              src={
                (Pokemon &&
                  Pokemon["sprites"] &&
                  Pokemon["sprites"]["front_default"]) ||
                "https://thumbs.dreamstime.com/z/icono-de-plantilla-error-lugar-muerto-p%C3%A1gina-no-encontrada-problemas-con-el-sistema-eps-164583533.jpg"
              }
            ></img>
          </div>
          <div className="col-lg-6 col-md-6 col-sm-12  bg-white border border-2 rounded-3">
            <span className="text-center m-3">
              <h3>Información:</h3>
            </span>
            <div className="d-flex justify-content-center m-5">
              <Stats data={Pokemon["stats"]}></Stats>
            </div>
            <div className="d-flex justify-content-center m-5">
            <StatsGraph data={Pokemon["stats"]}></StatsGraph>
          </div>
          </div>
        </div>
      
    </div>
  );
}

export default PokemonDetail;
