export const types = {
    'electric':'#FFFF00',
    'grass':'#7CFC00',
    'fire':'#ff5000',
    'water':'#00BFFF',
    'bug':'#6B8E23',
    'normal':'#DCDCDC',
    'ground':'#DEB887',
    'poison':'#DA70D6',
    'fairy':'#FFB6C1',
    'fighting':'#FF7F50',
    'psychic':'#E6BE8A',
    'rock':'#778899',
    'ghost':'#9370DB',
    'flying':'#d3d3d3'


}

export default types;