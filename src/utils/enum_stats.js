const stats = {
    'hp':'hp',
    'attack':'atk',
    'defense':'def',
    'special-attack':'spe-atk',
    'special-defense':'spe-def',
    'speed':'spd'
}

export default stats;