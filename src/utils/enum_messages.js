
export const messages = {
    0:' podria ser un buen compañero.',
    1:', quizás pueda ayudarte en tareas de la casa.',
    2:' es muy amigable, aunque un poco mañoso.',
    3:'... dicen que a veces puede ser problemático, pero no digas que te lo dije.',
    4:'... bueno, te diria que tengas un poco de cuidado.',
    5:', oh... claro, su anterior dueño... no te lo recomendaria.',
    6:'!! que excelente elección!',
    7:', yo... le preguntaria a tus padres primero.',
    8:' primero deberias conocer a sus amigos.',
    9:' y tú, se llevarian súper bien!!'
}

export default messages;