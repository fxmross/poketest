import React, { useEffect, useState } from "react";
import logo from "./logo.svg";
import "./App.scss";
import Axios from "axios";
import CardPokemon from "./components/card-pokemon/card-pokemon";
import Grid from "@material-ui/core/Grid";
import SearchIcon from "@material-ui/icons/Search";
import InfiniteScroll from "react-infinite-scroll-component";
import Modal from "@material-ui/core/Modal";
import Typography from "@material-ui/core/Typography";
import messages from "./utils/enum_messages";
import Oak from "./assets/profesoroak.png";
import { useNavigate } from 'react-router-dom';


function App () {
  const navigate = useNavigate();
  const [pokemons, setPokemons] = useState([])
  const [pokemonsTemp, setPokemonsTemp] = useState([])
  const [filterPokemons, setFilterPokemons] = useState([])
  const [Count, setCount] = useState(0)
  const [query, setQuery] = useState('')
  const [nextPage, setNextPage] = useState('')
  const [openModal, setOpenModal] = useState(false)
  const [selectedPokemon, setSelectedPokemon] = useState({})
  const [message, setMessage] = useState('')


  const getAllPokemons = async () => {
    try {
      let url = `https://pokeapi.co/api/v2/pokemon/?limit=${Count}`;
       
      const _pokemons = await Axios.get(`${url}`)
        .then((data) => {
          return data.data;
        })
        .catch((error) => console.log(error));
      if (_pokemons) {
        setPokemonsTemp([..._pokemons.results])
        setFilterPokemons([..._pokemons.results])
        setNextPage(_pokemons.next)
      }
    } catch (error) {
      console.error("fetchItems =>", error);
    }
  }
  const fetchItems = async () => {
    try {
      let url = "";
      if (nextPage !== "" && query === "") {
        url = nextPage;
      } else {
        url = "https://pokeapi.co/api/v2/pokemon/?limit=50&offset=0";
        setFilterPokemons([]);
        setPokemons([]);
        setQuery('');
      }
      const _pokemons = await Axios.get(`${url}`)
        .then((data) => {
          setCount(data.data.count)
          return data.data;
        })
        .catch((error) => console.log(error));
      if (_pokemons) {
        setPokemons([...pokemons,..._pokemons.results])
        setFilterPokemons([...pokemons,..._pokemons.results])
        setNextPage(_pokemons.next)
      }
    } catch (error) {
      console.error("fetchItems =>", error);
    }
  };

  const filterPokemon = async () => {
    setFilterPokemons([]);
    const url = `https://pokeapi.co/api/v2/pokemon/${query}`;
    let filtered = await Axios.get(`${url}`)
      .then((data) => {
        if (data && data.data) {
          setFilterPokemons([data.data]);
        }
      })
      .catch((error) => {
        return null;
      });
    if (filtered === null) {
      filtered = pokemons.filter((p, i) => {
        if (p.name.includes(query)) {
          return p;
        }
      });
      setFilterPokemons([...filtered]);
	  return;
    }
  };

  const handleOnChangeQuery = (e) => {
    setQuery(e.target.value)
  };
   const handleOpenModal = (data) => {
    let numb = Math.floor(Math.random() * (10 - 0)) + 0;
    setSelectedPokemon(data);
    setMessage(messages[numb]);
	setOpenModal(true);
  };
   const handleCloseModal = () => {
     setOpenModal(false)
  };

  useEffect(() => {
    fetchItems();
	getAllPokemons();
  }, []);
  

  
   
   const handleClickCard = (data) => {
    navigate(`/pokemon/${data}`);

  };

   
    return (
      <div className="App">
        <div className="App-header container-fluid">
          <div className="container">
            <div className="row">
              <div className="col-12 d-flex justify-content-start ">
                <div>
                  <p>Name or Number</p>
                  <span className="d-flex align-middle">
                    <input
                      className="w-100"
                      onChange={(e) => handleOnChangeQuery(e)}
                    ></input>{" "}
                    <button
                      className="btn btn-warning ms-2"
                      onClick={() => filterPokemon()}
                    >
                      <SearchIcon />
                    </button>
                  </span>
                </div>
              </div>
            </div>
          </div>
        </div>
       
        <div className="App-body mt-5">
          <div className="container">
           
            <div className="mt-5">
			{filterPokemons && filterPokemons.length > 0 ? (
				<InfiniteScroll
                dataLength={filterPokemons.length}
                next={()=>fetchItems()}
                hasMore={true}
                loader={
                  filterPokemons.length < 0 && <h4>Loading...</h4>
                }
                endMessage={
                  <p style={{ textAlign: "center" }}>
                    <b>Ya no tenemos más opciones</b>
                  </p>
                }
                refreshFunction={()=>fetchItems()}
                pullDownToRefresh
                pullDownToRefreshThreshold={150}
                pullDownToRefreshContent={
                  <h3 style={{ textAlign: "center" }}>
                    &#8595; Pull down to refresh
                  </h3>
                }
                releaseToRefreshContent={
                  <h3 style={{ textAlign: "center" }}>
                    &#8593; Release to refresh
                  </h3>
                }
              >
                <Grid container spacing={2}>
                  {filterPokemons.length > 0 &&
                    filterPokemons.map((p, i) => {
                      return (
                        <Grid item xs={12} sm={12} md={4} key={i}>
                          <CardPokemon
                            id={p.name}
                            key={i}
                            onClick={(id)=>handleClickCard(id)}
							openModal={(data)=>handleOpenModal(data)}
                          ></CardPokemon>
                        </Grid>
                      );
                    })}
                </Grid>
              </InfiniteScroll>

			) : (
				<p style={{ textAlign: "center" }}>
				<b>Al parecer, no has buscado con sabiduría.</b>
			  </p>

			) }
             
            </div>
          </div>
        </div>
        <Modal
          open={openModal}
          onClose={handleCloseModal}
          aria-labelledby="modal-modal-title"
          aria-describedby="modal-modal-description"
        >
          <div className="container mt-5 modal-container">
		  <div className="modal-content text-center p-5">
            <Typography id="modal-modal-title" variant="h6" component="h2">
              {selectedPokemon && selectedPokemon["name"]}
              {message}
            </Typography>
            <span className="d-flex justify-content-center"><img src={Oak} width={300} height={500} alt="oak"></img></span>
            <Typography
              id="modal-modal-description"
              sx={{ mt: 2 }}
            ></Typography>
			<div className="d-flex justify-content-center my-2 w-100">
			<button className="btn btn-danger w-100" onClick={()=>handleCloseModal()}>Cerrar</button></div>
			</div>
          </div>
        </Modal>
      </div>
    );
  
}

export default App;
