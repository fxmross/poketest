import React, { useEffect, useState } from "react";
import stats from "../../utils/enum_stats";
import './stats.scss';

const enum_color = {
  'hp':'#32a852',
  'attack':'#a83232',
  'defense':'#0000ed',
  'special-attack':'#630000',
  'special-defense':'#0c0063',
  'speed':'#00a0eb'
  }

  
function Stats(props) {
  const { data } = props;
  return (
    <div className={"stats_container"}>
      {data && (
        <>
          <span>
            <p style={{backgroundColor:enum_color[data[0].stat.name]}}>
              {stats[data[0].stat.name]} - {data[0].base_stat}
            </p>
            <p style={{backgroundColor:enum_color[data[1].stat.name]}}>
              {stats[data[1].stat.name]} - {data[1].base_stat}
            </p>
            <p style={{backgroundColor:enum_color[data[2].stat.name]}}>
              {stats[data[2].stat.name]} - {data[2].base_stat}
            </p>
          </span>
          <span></span>
          <span>
            <p style={{backgroundColor:enum_color[data[3].stat.name]}}>
              {stats[data[3].stat.name]} - {data[3].base_stat}
            </p>
            <p style={{backgroundColor:enum_color[data[4].stat.name]}}>
              {stats[data[4].stat.name]} - {data[4].base_stat}
            </p>
            <p style={{backgroundColor:enum_color[data[5].stat.name]}}>
              {stats[data[5].stat.name]} - {data[5].base_stat}
            </p>
          </span>
        </>
      )}
    </div>
  );
}

export default Stats;
