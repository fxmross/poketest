import React, { useEffect, useState } from "react";
import Axios from "axios";
import Card from "@material-ui/core/Card";
import CardActionArea from "@material-ui/core/CardActionArea";
import CardActions from "@material-ui/core/CardActions";
import CardContent from "@material-ui/core/CardContent";
import CardMedia from "@material-ui/core/CardMedia";
import Button from "@material-ui/core/Button";
import Typography from "@material-ui/core/Typography";
import types from "../../utils/enum_type";

import "./card-pokemon.scss";
function CardPokemon(props) {
  const [pokemon, setPokemon] = useState({});
  const getInfoPokemon = (payload) => {
    Axios.get(`https://pokeapi.co/api/v2/pokemon/${payload}`)
      .then((data) => {
        setPokemon(data.data);
      })
      .catch((error) => console.log(error));
  };
  useEffect(() => {
    getInfoPokemon(props.id);
  }, []);

  const renderTypes = () => {
    const _types = pokemon.types;
    return (
      <div className="d-flex">
        {_types &&
          _types.map((t, i) => {
            return (
              <Typography
                key={i}
                className={`cardPoke_container_types`}
                component="div"
                style={{
                  backgroundColor: `${types[t["type"]["name"]]}`,
                  marginRight: ".5rem",
                }}
              >
                {t["type"]["name"]}
              </Typography>
            );
          })}
      </div>
    );
  };

  const renderId = () => {
    let tempId = "" + pokemon.id;
    for (var i = 3; i >= tempId.length; i--) {
      tempId = "0" + tempId;
    }
    return tempId;
  };

  const handleOnClick = () => {
      props.onClick(pokemon.id);
  }

  const handleOpenModal = (e) => {
    e.stopPropagation();
    props.openModal(pokemon);
  }
  return (
    <div className={`cardPoke_container`}>
      <Card onClick={() => handleOnClick()}>
        <CardMedia
          component="img"
          height="140"
          image={
            (pokemon &&
              pokemon["sprites"] &&
              pokemon["sprites"]["front_default"]) ||
            "https://thumbs.dreamstime.com/z/icono-de-plantilla-error-lugar-muerto-p%C3%A1gina-no-encontrada-problemas-con-el-sistema-eps-164583533.jpg"
          }
          alt={pokemon["name"]}
          className={`cardPoke_container_card_media`}
        ></CardMedia>
        <CardContent className={`cardPoke_container_content`}>
          <Typography
            gutterBottom
            variant="body1"
            component="div"
            className={`cardPoke_container_id`}
          >
            #{renderId()}
          </Typography>
          <Typography
            gutterBottom
            variant="h5"
            component="div"
            className={`cardPoke_container_name`}
          >
            {pokemon["name"]}
          </Typography>
          {renderTypes()}
         
        </CardContent>
        <div className="d-flex justify-content-end bg-white">
            <button className="btn btn-warning" onClick={(e)=>handleOpenModal(e)}> Que dice Oak?</button></div>
      </Card>
    </div>
  );
}

export default CardPokemon;
