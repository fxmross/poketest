import { Group } from "@visx/group";
import { Pie } from "@visx/shape";
import React, { useState, useMemo } from "react";

const enum_color = {
'hp':'#32a852',
'attack':'#a83232',
'defense':'#0000ed',
'special-attack':'#630000',
'special-defense':'#0c0063',
'speed':'#00a0eb'
}
function StatsGraph(props) {
  const { data } = props;
  const dataNormal = data && data.map((st)=>{
      return {...st, color:enum_color[st.stat.name]}
  })
  const width = window.innerWidth / 3;
  const half = width / 2;
  return (
    <div>
      <svg width={width} height={width}>
        <Group top={half} left={half}>
          <Pie
            data={dataNormal}
            pieValue={(data) => data.base_stat}
            outerRadius={half}
            innerRadius={half - 40}
            padAngle={0.02}
          >
          {
              (pie)=> {
                  return pie.arcs.map((arc,i)=>{
                      return (
                          <g key={i}>
                          <path d={pie.path(arc)} fill={arc.data.color}></path>
                          </g>
                      )
                  })
              }
          }
              </Pie>
        </Group>
      </svg>
    </div>
  );
}
export default StatsGraph;
