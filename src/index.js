import React from "react";
import ReactDOM from "react-dom";
import { BrowserRouter, Routes, Route } from "react-router-dom";
import "./index.css";
import App from "./App";
import "bootstrap/dist/css/bootstrap.css";
import PokemonDetail from "./pages/pokemon-detail/pokemon-detail";
import { createBrowserHistory } from "history";

const history = createBrowserHistory();
ReactDOM.render(
  <BrowserRouter history={history}>
    <Routes>
      <Route path="/" element={<App />} />
      <Route path="pokemon/:id" element={<PokemonDetail />} />
    </Routes>
  </BrowserRouter>,
  document.getElementById("root")
);
